# README #

This README is a guide on what we worked on for the repository using git and bitbucket.

### What is this repository for? ###

* This repository is to explain how repositories work. It is a collaboration from different group members that explain how changes can be made by each person using different git commands. 
* Current Version
* https://bitbucket.org/lis4381_fall14_a4_team1/assignment4

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

Joey worked on git commands/respository.
Mauricio worked on git commands/questions.
Juan worked on git commands/powerpoint.
Ania worked on git commands/questions.
Dakota worked on content for the repository.

### Who do I talk to? ###

* Juan Gomez
* Joey Barrentine
* Mauricio Gomez
* Ania Augustin
* Dakota Morrison